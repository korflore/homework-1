#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    //ofEnableAntiAliasing();
    ofSetCircleResolution(200);
    ofSetBackgroundColor(130);
    ofEnableSmoothing();
}

//--------------------------------------------------------------
void ofApp::update(){

}

//--------------------------------------------------------------
void ofApp::draw(){
    arms();
    neck();
    head();
    body();
    flame();

}

void ofApp::neck(){
    ofSetColor(0, 0, 0);
    ofDrawCircle(500, 400, 40);
}

void ofApp::arms(){
    //stroke(0);
    ofSetLineWidth(20);
    ofSetColor(155);
    ofDrawLine(350,425,275,700);
    ofDrawLine(650,425,725,700);
    ofSetLineWidth(5);
    //stroke(0);
    ofSetColor(0,0,0);
    ofDrawCircle(350, 425, 30);
    ofDrawCircle(650, 425, 30);
    ofDrawCircle(275,700,40);
    ofDrawCircle(725,700,40);
}

void ofApp::head(){
    ofSetColor(mouseX, mouseY, 20);
    ofDrawCircle(500, 285, 110);
      //continue head portion
    ofSetColor(mouseY, mouseX, 50);
    ofDrawCircle(500, 285, 80);
    ofSetColor(mouseX, mouseY, 100);
    ofDrawCircle(500, 285, 50);
    ofSetColor(mouseY, mouseX, 200);
    ofDrawCircle(500, 285, 20);
    ofDrawCircle(500, 530, 10);
    ofNoFill();
    ofSetColor(0);
    ofDrawCircle(500, 285, 110);
    ofDrawCircle(500, 285, 80);
    ofDrawCircle(500, 285, 50);
    ofDrawCircle(500, 285, 20);
    ofDrawCircle(500, 530, 10);
    ofFill();
}

void ofApp::body(){
    ofSetColor(0, 0, 0);
    ofDrawCircle(500, 700, 60);
    ofSetColor(mouseX, mouseY, 20);
    ofDrawRectRounded(350, 410, 300, 300, 10);

    ofSetColor(mouseY,mouseX,(mouseY+mouseX)/2);
    ofDrawCircle(500, 530, 50);
    ofNoFill();
    ofSetColor(0);
    ofDrawCircle(500, 530, 30);
    ofDrawCircle(500, 530, 15);
    ofDrawCircle(500, 530, 5);
    ofFill();
    ofSetColor(255, 0, 0);
    ofDrawCircle(400, 650, 15);
    ofDrawCircle(500, 650, 15);
    ofDrawCircle(600, 650, 15);
    ofNoFill();
    ofSetColor(0);
    ofDrawCircle(500, 530, 50);
    ofDrawRectRounded(350, 410, 300, 300, 10);
    ofDrawCircle(400, 650, 15);
    ofDrawCircle(500, 650, 15);
    ofDrawCircle(600, 650, 15);
    ofFill();
}

void ofApp::flame(){
    ofSetColor(255,0,0);
    ofDrawCurve(0, -500, 470, 800, 530, 800, 0, -500);
    
    
}
